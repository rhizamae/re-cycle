/**
 * Development environment settings
 *
 * This file can include shared settings for a development team,
 * such as API keys or remote database passwords.  If you're using
 * a version control solution for your Sails app, this file will
 * be committed to your repository unless you add it to your .gitignore
 * file.  If your repository will be publicly viewable, don't add
 * any private information to this file!
 *
 */

module.exports = {

  /***************************************************************************
   * Set the default database connection for models in the development       *
   * environment (see config/connections.js and config/models.js )           *
   ***************************************************************************/

  token: {
    mode: 'aes-128-cfb',
    key: 'CVj0O2WK1bd9mrR4',
    iv: 'dxm7k12FLLgd0U95',
    lifetime: 3600, // 86400  default
    client: {
      lifetime: 3600 // 86400
    }
  },

  gcm: {
    key: 'AIzaSyCI7JgP0lHRbQ-JFUr-LEbxKROofZ2FIr0',
    sender_id: '903489015119'
  }

};
