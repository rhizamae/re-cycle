/**
 * isAuthenticated
 *
 * @module      :: Policy
 * @description :: Simple policy to allow any authenticated user
 *                 Assumes that your login action in one of your controllers sets `req.session.authenticated = true;`
 * @docs        :: http://sailsjs.org/#!documentation/policies
 *
 */

var TAG = '[isAuthenticated]';

module.exports = function(req, res, next) {
  if ((typeof req.headers !== 'undefined') && (typeof req.headers.token !== 'undefined')) {
    try {
      var client_token = TokenService.getTokenizer().validate('client_token', req.headers.token);
      Logger.log('info', TAG + '[token] Accessed by', client_token.id);
      req.user = {id: client_token.id};
      return next();
    } catch (e) {
      var error = {};
      if (e instanceof TokenService.getTokenizer().MalformedToken) {
        error = ErrorService.raise('MALFORMED_TOKEN');
      } else if (e instanceof TokenService.getTokenizer().TokenExpired) {
        error = Errors.raise('EXPIRED_TOKEN');
      } else {
        error = ErrorService.raise('MALFORMED_TOKEN');
      }
      return res.json(error.status, {error: error.error});
    }
  } else {
    var error = ErrorService.raise('UNAUTHORIZED');
    return res.json(error.status, {error: error.error});
  }

};
