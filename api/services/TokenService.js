var TokenObj = require('../../lib/Token');
var Tokenizer;

module.exports = {

  getTokenizer: function () {
    if (!Tokenizer) {
      Tokenizer = new TokenObj(sails.config.token);
    }
    return Tokenizer;
  },

  generateToken: function(type, content, callback) {
    var _this = this;
    try {
      var token = _this.getTokenizer().generate(type, content);
      callback(null, token);
    } catch (e) {
      sails.log.error(e);
      var error = {};
      if (e instanceof _this.getTokenizer().MalformedToken) {
        error = ErrorService.raise('MALFORMED_TOKEN');
      } else if (e instanceof _this.getTokenizer().TokenExpired) {
        error = ErrorService.raise('EXPIRED_TOKEN');
      } else {
        error = ErrorService.raise('MALFORMED_TOKEN');
      }
      callback(error);
    }
  }
}

