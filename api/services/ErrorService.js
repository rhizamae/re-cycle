module.exports = {
  get: function (tag) {
    var errors = {
      MISSING_INVALID_PARAMS: {status: 400, error: {code: -1, msg: 'Missing/invalid parameters.', 'params': []}},
      DUPLICATE: {status: 400, error: {code: -2, msg: 'Duplicate record.', spiel: 'Duplicate record.'}},
      MALFORMED_TOKEN: {
        status: 401,
        error: {code: -3, msg: 'Not authorized. Malformed token.', spiel: this.getSpiel('UNAUTHORIZED')}
      },
      EXPIRED_TOKEN: {
        status: 401,
        error: {code: -4, msg: 'Not authorized. Token already expired.', spiel: this.getSpiel('UNAUTHORIZED')}
      },
      UNAUTHORIZED: {status: 401, error: {code: -5, msg: 'Unauthorized.', spiel: this.getSpiel('UNAUTHORIZED')}},
      NOT_FOUND: { status: 404, error: {code: -6, msg: 'Not found.', spiel: 'Not found.'}},
      INTERNAL_SERVER_ERROR: {
        status: 500,
        error: {code: -7, msg: 'Internal server error.', spiel: this.getSpiel('SERVICE_ERROR')}
      },
      DB_ERROR: {
        status: 503,
        error: {code: -8, msg: 'Database error/unavailable.', spiel: this.getSpiel('SERVICE_ERROR')}
      }
    };
    return errors[tag];
  },
  raise: function (e) {
    var error = JSON.parse(JSON.stringify(this.get(e)));
    return error;
  },
  getDetail: function (code) {
    var detail;
    var errors = this.errors();
    for (var error in errors) {
      if (errors[error]['error']['code'] == code) {
        detail = errors[error];
      }
    }
    return detail;
  },
  getParam: function (tag) {
    var params = {
      vcode: {field: 'vcode', desc: 'Vcode must be a string.'},
    };
    return params[tag];
  },

  getSpiel: function (tag) {
    var spiels = {
      SERVICE_UNAVAILABLE: 'We can\'t connect you to the service at the moment, please try again in a few minutes.',
      SERVICE_ERROR: 'We had a problem processing your request, please try again in a few minutes.',
      UNAUTHORIZED: 'Your session has expired, please log in again.',
      INVALID_LOGIN: 'Username and password do not match. Please try again. If username and password do not work, your account may have been disabled.',
    };
    return spiels[tag];
  }
};
