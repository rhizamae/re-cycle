var shortId = require('shortid');
var path = require("path");
var mkdirp = require("mkdirp");
var fs = require("fs");
var graph = require("fbgraph");

module.exports = {
  share: function (option, callback) {
    var options = {
      url: option.picture,
      message: option.message,
      no_story: false,
      access_token: option.access_token
    }

    var tid = shortId.generate();

    var tasks = {

      processPicture: function(next) {
        if (!option.picture) next(null, true);
        processPhoto(options, tid, function(err, photo) {
          if (err || !photo) {
            return next(err);
          }
          if (photo) {
            options.url = photo.src
            next(null, true);
          }
        });
      },

      toUrl: ["processPicture", function(next) {
        if (!option.picture) next(null, true);
        options.url = "http://52.76.93.75:1330/images/" + tid + "/out.jpeg";
        next(null, options);
      }],

      postToFb: ["toUrl", function(next) {
        graph.post("me/photos",
          options,
          function (err, res) {
          if (!res) {
            return next({message: "Invalid Access Token"});
          } else {
            return next(null, res);
          }
        });
      }]
    }

    async.auto(tasks, function(err, result) {
      if (err) {
        callback(err);
      } else {
        callback(null, result);
      }
    });
  }
}

function processPhoto(options, id, callback) {
  if (options.url) {
    var tpath = "./assets/images/" + id + "/out.jpeg";
    base64ToFile(options.url, tpath, id, function(err, path) {
      if (err) callback({
        message: "Uploading photo failed!"
      });
      var src = "http://52.76.93.75:1330" + tpath;
      callback(null, {
        src: src
      });
    });


  } else {
    callback();
  }
}

function base64ToFile(base64, fpath, id, done) {
  mkdirp.sync(path.dirname(fpath), 0755);
  base64 = base64.replace(/^data:[A-Za-z\/]+;base64,/, "");
  fs.writeFile(fpath, base64, "base64", done);
}
