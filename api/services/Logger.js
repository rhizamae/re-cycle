module.exports = {
  log: function(type, tag, body) { 
    body = this.formatObject(body);
    var date = new Date().toISOString();
    switch (type) {
      case 'debug' :
        sails.log.debug(date, tag + ' :\n', body);
        break;
      case 'error' :
        sails.log.error(date, tag + ' error :\n', body);
        break;
      default :
        sails.log.info(date, tag + ' :\n', body);
    }
  },

  formatObject: function(obj) {
    if ((typeof obj) == 'object' && !Array.isArray(obj) && obj !== null) {
      obj = JSON.parse(JSON.stringify(obj));
      if (obj.password) {
        obj.password = '[HIDDEN]';
      }
      obj = JSON.stringify(obj);
    } 
    if (Array.isArray(obj) && obj.length > 0) {
      obj = JSON.stringify(obj);
    }
    return obj;
  }
}