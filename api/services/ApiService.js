
module.exports = {
  
  toSuccessJSON: function (data) {
    var payload = {
      status: "success",
      data: data || null
    }

    return payload;
  },

  toErrorJSON: function (data) {
    var payload = {
      status: "error",
      data: data || null
    }
    
    return payload;
  }
}