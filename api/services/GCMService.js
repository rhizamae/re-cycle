var TAG = '[GCMService]';
var GCM = require('gcm').GCM;
var node_gcm = require('node-gcm');

module.exports = {
  send1: function(msg, registration_id) {
    var ACTION = '[send]';
    var gcm = new GCM(sails.config.gcm.key);
    var message = {
      registration_id: registration_id,
      priority: 'high',
      content_available: true,
      delay_while_idle: true,
      collapse_key: 'Collapse key', 
      'data.message': msg
    };

    gcm.send(message, function(err, messageId) {
      if (err) {
        Logger.log('error', TAG + ACTION, err);
      } else {
        Logger.log('debug', TAG + ACTION, {reg_id: registration_id, "Sent with message ID": messageId, message: msg});
      }
    });
  },

  send: function(msg, registration_id) {
    var ACTION = '[send]';
    var message = new node_gcm.Message();
    message.addData('message', msg);
    var sender = new node_gcm.Sender(sails.config.gcm.key);
    sender.send(message, registration_id, function (err, result) {
      if (err) {
        Logger.log('error', TAG + ACTION, err);
      } else {
        Logger.log('debug', TAG + ACTION, result);
      }
    });
  }
}