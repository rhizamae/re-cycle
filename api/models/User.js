/**
 * Verification
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs    :: http://sailsjs.org/#!documentation/models 
 */

module.exports = {
  connection: "mongodb",
  autoCreatedAt: false,
  autoUpdatedAt: false,
  autoPK: false,
  attributes: {
    id: {
      type: 'string',
      required: true,
      unique: true
    },
    fb_access_token: {
      type: 'string',
      required: true
    },
    name: {
      type: 'string',
      required: true
    },
    image_url: {
      type: 'string',
      required: false
    },
    greenies: {
      type: 'float',
      defaultsTo: 0
    },
    carbon: {
      type: 'float',
      defaultsTo: 0
    },
    distance: {
      string: 'float',
      defaultsTo: 0
    },
  }
};
