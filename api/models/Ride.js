
module.exports = {
  
  tableName: "rides",

  attributes: {
    code: {
      type: "string",
      required: true
    },
    masterId: {
      type: "string",
      required: true
    },
    members: {
      type: "array",
      defaultsTo: []
    },
    totalDistance: {
      type: "float",
      defaultsTo: 0
    }
  }
}