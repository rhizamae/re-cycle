var TAG = '[SessionController]';

module.exports = {
  login: function (req, res) {
    res.view('login', {status: 'success', data: {}});
  },

  redirect: function (req, res) {
    res.json(200, {status: 'success', data: {}});
  }
}