var shortid = require("shortid");

module.exports = {
  start: function (req, res) {
    var payload = {};
    var code = req.param("code");
    var tasks = {
      find: function (next) {
        if (code) {
          Ride.findOne({code: code}).exec(function(err, ride) {
            if (err) {
              sails.log.error("Ride.start: ", err);
            }
            err = !ride ? ErrorService.raise('NOT_FOUND') : undefined;
            next(err, ride);
          });
        } else {
          next(null, {members: []});
        }
      }
    };

    async.auto(tasks, function (err, result) {
      var payload = {};
      if (err) {
        payload = ApiService.toErrorJSON(err);
      } else {
        payload = ApiService.toSuccessJSON({message: "Successfully started"});
        result.find.members.forEach(function(member) {
          GCMService.send("Successfully started", member.reg_id);
        });
      }
      res.json(200, payload);
    });
  },

  generateCode: function (req, res) {
    var code = (shortid.generate()).toUpperCase();
    var params = {
      code: code,
      masterId: req.user.id,
    };

    Ride.create(params, function (err, ride) {
      var payload = {};
      if (err) {
        sails.log.error("Ride.generateCode: ", err);
        payload = ApiService.toErrorJSON(err);
      } else {
        sails.log.debug("Ride.generateCode: ", ride);
        payload = ApiService.toSuccessJSON({code: code});
      }
      res.json(200, payload);
    });
  },

  join: function (req, res) {
    var code = req.param("code");
    var reg_id = req.param("reg_id");
    var member_id = req.user.id;

    var tasks = {
      find: function (next) {
        Ride.findOne({code: code}).exec(function (err, ride) {
          if (err) {
            sails.log.error("Ride.join find: ", err);
          } else {
            err = !ride ? ErrorService.raise('NOT_FOUND') : undefined;
            sails.log.debug("Ride.join find: ", ride);
          }
          next(err, ride);
        });
      },
      update: ["find", function (next, result) {
        if (result.find) {
          console.log("has find");
          result.find.members.push({
            id: member_id,
            pic_url: "http://graph.facebook.com/" + member_id + "/picture",
            reg_id: reg_id
          });
          result.find.save(next);
        }
        next();
      }]
    };

    async.auto(tasks, function (err, result) {
      var payload = {};
      if (err) {
        payload = ApiService.toErrorJSON(err);
      } else {
        payload = ApiService.toSuccessJSON({message: "Successfully joined"});
      }
      res.json(200, payload);
    });
  },

  stop: function (req, res) {
    var code = req.param("code");
    var distance = req.param("total_distance");

    var params = {
      totalDistance: distance,
      updatedAt: new Date()
    };

    var tasks = {
      ride: function (next) {
        Ride.update({code: code}, params).exec(function (err, ride) {
          if (err) {
            sails.log.error("Ride.stop find: ", err);
          } else {
            sails.log.debug("Ride.stop find: ", ride);
            err = ride.length === 0 ? ErrorService.raise('NOT_FOUND') : undefined;
          }
          next(err, ride[0]);
        });
      },
      speed: ["ride", function (next, result) {
        var time_difference = Math.abs(result.ride.updatedAt - result.ride.createdAt);
        var hour = 0.000000277778 * time_difference;
        var average_speed = Math.round((distance / hour) * 100)/100 + " km/h";
        next(null, average_speed);
      }],
      carbon: ["ride", function (next, result) {
        var carbon_reduced = (distance/1000) + " kg";
        next(null, carbon_reduced);
      }],
      greenie: ["ride", function (next, result) {
        var greenie = Math.floor(distance/10);
        var challenge_bonus_greenie = req.param("challenge_bonus") || 0;
        var bonus_greenie = 0;

        if (result.ride.members.length > 0) {
          var friends_joined = result.ride.members.length;
          if (friends_joined > 0 && friends_joined <= 4) bonus_greenie = 2;
          else if (friends_joined > 4 && friends_joined <= 10) bonus_greenie = 3;
          else if (friends_joined > 10) bonus_greenie = 4;
        }

        var total_greenies_gained = greenie + challenge_bonus_greenie + bonus_greenie;
        
        next(null, {
          greenie: greenie,
          bonus_greenie: bonus_greenie,
          challenge_bonus_greenie: challenge_bonus_greenie,
          total_greenies_gained: total_greenies_gained
        })
      }]
    }

    async.auto(tasks, function (err, result) {
      var payload = {};
      if (err) {
        payload = ApiService.toErrorJSON(err);
      } else {
        payload = ApiService.toSuccessJSON({
          message: "Successfully stopped",
          carbon_reduced: result.carbon,
          distance_travelled: distance + " km",
          average_speed: result.speed,
          friends_joined: result.ride.members,
          greenies: result.greenie
        });
        if (result.ride.masterId == req.user.id) {
          result.ride.members.forEach(function(member) {
            GCMService.send("Successfully stopped", member.reg_id);
          });
        }
      }
      res.json(200, payload);
    });
  },

  share: function(req, res) {
    console.log("====== share ===");
    var params = {
      picture: req.param("picture"),
      message: req.param("message")
    };

    console.log("params: ",params);
    var tasks = {
      find: function (next) {
        User.findOne({id: req.param("id")}, function(err, data) {
          console.log("err: ",err);
          console.log("data: ",data);
          if (err) {
            next(ErrorService.raise('DB_ERROR'));
          } else {
            next(null, data);
          }
        });  
      },
      share: ["find", function (next, result) {
        params.access_token = result.find.fb_access_token || req.headers.token;
        console.log("params: ",params);
        FacebookService.share(params,function (err, result) {
          if (err) {
            sails.log.error("Social.share: ", err);
          } else {
            sails.log.debug("Social.share: ", result);
          }
          next(err, result);
        });
      }]
    }

    async.auto(tasks, function (err, result) {
      var payload = {};
      if (err) {
        payload = ApiService.toErrorJSON(err);
      } else {
        payload = ApiService.toSuccessJSON({message: "Successfully shared"});
      }
      res.json(200, payload);
    });
  }
}