var TAG = '[UserController]';

module.exports = {

  create: function(req, res) {
    var ACTION = '[create]';
    async.auto({
      find: function(callback) {
        User.find({id: req.body.id}, function(err, data) {
          if (err) {
            Logger.log('error', TAG + ACTION + ' database', err);
            callback(ErrorService.raise('DB_ERROR'));
          } else {
            callback(null, data);
          }
        });
      },
      create: ['find', function(callback, result) {
        if (result.find.length == 0) {
          User.create(req.body, function(err, data) {
            if (err) {
              Logger.log('error', TAG + ACTION + ' database', err);
              callback(ErrorService.raise('DB_ERROR'));
            } else {
              callback(null, data);
            }
          });
        } else {
          User.update({id: req.body.id}, {fb_access_token: req.body.fb_access_token}).exec(function (err, data) {
            if (err) {
              Logger.log('error', TAG + ACTION + ' database', err);
              callback(ErrorService.raise('DB_ERROR'));
            } else {
              callback(null, data[0]);
            }
          });
        }
      }],
      token: ['create', function(callback, result) {
        TokenService.generateToken('client_token', { id: req.body.id }, function(err, token) {
          if (err) {
            Logger.log('error', TAG + ACTION + ' token', err);
            callback(ErrorService.raise('INTERNAL_SERVER_ERROR'));
          } else {
            callback(null, token);
          }
        });
      }]
    }, function(err, result) {
      var msg = {};
      if (err) {
        Logger.log('error', TAG + ACTION + ' database', err);
        res.json(err.status, err.error);
      } else {
        result.create.token = result.token;
        res.json(200, {status: 'success', data: result.create});
      }
    });
  },

  show: function(req, res) {
    var ACTION = '[show]';
    User.find({id: req.user.id}, function(err, data) {
      if (err) {
        Logger.log('error', TAG + ACTION + ' database', err);
        err = ErrorService.raise('DB_ERROR');
        res.json(err.status, err.error);
      } else {
        res.json(200, {status: 'success', data: data[0]});
      }
    });
  }

}